# Elinver Assessment
## The key use of docker
* Docker brings security to applications running in a shared environment.
* Faster development process.
* Easy to scale and monitor.
* Ensuring a consistent environment for new builds.
* Open source, so it’s free to use.
## tools
* Docker
* Docker Compose
* GitLab
* Kubernetes Cluster for staging & prod
* GCP Container Registry
* Google Service account

## Things that has been done.
* Backend, Frontend & Cypress test has been dockerized individually. so that it can be modified & updated without any dependencies.
* This docker can use to run unit & integration test separately for frontend & backend.

## Getting started

Download [Docker Desktop](https://www.docker.com/products/docker-desktop) for Mac or Windows. [Docker Compose](https://docs.docker.com/compose) will be automatically installed. On Linux, make sure you have the latest version of [Compose](https://docs.docker.com/compose/install/). 

#### Kubernetes Cluster setup:
  
  Create two cluster with environment as staging & production using [kubernetes integration](https://about.gitlab.com/blog/2020/03/27/gitlab-ci-on-google-kubernetes-engine/). 

#### Run in this directory:
```
docker-compose up
```
* The app backend will be running at [http://localhost:5000](http://localhost:5000), and the results will be at [http://localhost:5000](http://localhost:5000).
* The app Frontend will be running at [http://localhost:3000](http://localhost:3000), and the results will be at [http://localhost:3000](http://localhost:3000).

#### Run test in this directory:
* Chrome
```
docker-compose run cypresstest npm run cypress:chrome
```
* Firefox
```
docker-compose run cypresstest npm run cypress:firefox
```
* Edge
```
docker-compose run cypresstestedge npm run cypress:edge
```
#### Communication between container
* we build all the images & containers using docker compose. So the containers will be under same network by default. 
might be like 'ta_assignment_default'.Get the network details by using below code.
```
docker network ls 
```  
* Inorder to access localhost:port of one container from another using http://<container ip>:port or http://<container name>:port. 
* Use the below code to get the ip address of a container.
```
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id
```
* Frontend docker file will modify the ip address of backend service in package.json
* Cypress docker file will modify the ip address of base_url in the cypress.json
#### Docker push into GCP Container Registry:
* Convert Json key generated file to Base64. So it will be added as environmental variable for the pipeline.
```
base64 key.json
```
* Store the token as environmental variable as 'GCP_SA_KEY' in gitlab.
```
# Install CA certs, openssl to https downloads, python for gcloud sdk
- apk add --update make ca-certificates openssl python
- update-ca-certificates
# Write our GCP service account private key into a file
- echo $GCP_SA_KEY | base64 -d > ${HOME}/gcloud-service-key.json
# Download and install Google Cloud SDK
- wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz
- tar zxvf google-cloud-sdk.tar.gz && ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true
- google-cloud-sdk/bin/gcloud --quiet components update
- google-cloud-sdk/bin/gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
- docker login -u _json_key --password-stdin https://gcr.io < ${HOME}/gcloud-service-key.json
```
## FrontEnd Docker File:
```
FROM node:13.12.0-alpine
COPY . /app
WORKDIR /app
RUN npm install -g json
ARG API_URL
RUN json -I -f package.json -e "this.proxy='${API_URL}'"
ENV PATH /app/node_modules/.bin:$PATH
RUN npm install  --force --silent
RUN npm install react-scripts@3.4.1 -g --silent
CMD npm start
```
## Backend Docker File:
```
FROM python:alpine3.7 
COPY ./api /app
WORKDIR /app
RUN pip install -r requirements.txt 
EXPOSE 5000
CMD flask run --host=0.0.0.0
```
## Cypress Docker File:
```
ARG IMAGE
FROM cypress/included:${IMAGE}
COPY . /app
WORKDIR /app
RUN npm install  --force --silent
RUN apt-get update
RUN yes Y | apt-get install libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
RUN npm install -g json
ARG BASE_URL
RUN json -I -f cypress.json -e "this.baseUrl='${BASE_URL}'"
EXPOSE 22
```
## Cypress Edge Docker File:
```
ARG IMAGE
FROM cypress/browsers:${IMAGE}
COPY . /app
WORKDIR /app
RUN npm install  --force --silent
RUN npm i cypress
RUN apt-get update
RUN yes Y | apt-get install libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
RUN npm install -g json
ARG BASE_URL
RUN json -I -f cypress.json -e "this.baseUrl='${BASE_URL}'"
EXPOSE 22
```
## Docker-compose File:
```
version: "3.3"
services:
  cypresstest:
    depends_on:
      - ui
      - backend 
    container_name: cypresstest
    build:
      context: .
      dockerfile: CypressDocker
      args:
        IMAGE: 6.2.1
        BASE_URL: http://ui:3000/#
    links:
      - ui:ui
    depends_on:
      - ui
      - apiservice  
  cypresstestedge:
    depends_on:
      - ui
      - apiservice 
    container_name: cypresstestedge
    build:
      context: .
      dockerfile: CypressEdge
      args:
        IMAGE: node14.10.1-edge88
        BASE_URL: http://ui:3000/#
    links:
      - ui:ui
  ui:
    restart: always
    container_name: ui
    build:
      context: .
      dockerfile: FrontendDocker
      args:
       API_URL: http://apiservice:5000
    stdin_open: true
    ports:
      - 3000:3000
    expose:
      - 3000
    links:
      - apiservice
    environment:
      - DANGEROUSLY_DISABLE_HOST_CHECK=true
      
  apiservice:
    restart: always
    container_name: apiservice
    build:
      context: .
      dockerfile: BackendDocker
    ports:
      - 5000:5000
    expose:
      - 5000
    environment:
      - FLASK_ENV=development
      - FLASK_APP=app.py
      - FLASK_DEBUG=1
```
## Gitlab-ci.yml File :
```
image: docker:18.09.7-dind

services:
  - docker:18.09.7-dind

stages:
  - prepare
  - test
  - publishing_docker
  - deploy

variables:
  DOCKER_DRIVER: overlay
  DOCKER_HOST: tcp://localhost:2375

UI Docker:
  stage: prepare
  environment: staging
  before_script:
    - apk add 'py-pip'
    - pip install 'docker-compose==1.22.0'
    - docker-compose version
  script:
    - docker-compose build ui
    - docker save -o ta_assignment_ui.tar ta_assignment_ui
  artifacts:
    name: "report"
    expire_in: 1 day
    paths: 
      - ta_assignment_ui.tar
Backend Docker:
  stage: prepare
  environment: staging
  before_script:
    - apk add 'py-pip'
    - pip install 'docker-compose==1.22.0'
    - docker-compose version
  script:
    - docker-compose build apiservice
    - docker save -o ta_assignment_apiservice.tar ta_assignment_apiservice
  artifacts:
    name: "report"
    expire_in: 1 day
    paths: 
      - ta_assignment_apiservice.tar

Chrome Test:
  stage: test
  environment: staging
  before_script:
    - apk add 'py-pip'
    - pip install 'docker-compose==1.22.0'
    - docker-compose version
    - docker load -i ta_assignment_apiservice.tar
    - docker load -i ta_assignment_ui.tar
  script:
    - docker-compose run cypresstest npm run cypress:chrome

Firefox Test:
  stage: test
  environment: staging
  before_script:
    - apk add 'py-pip'
    - pip install 'docker-compose==1.22.0'
    - docker-compose version
    - docker load -i ta_assignment_apiservice.tar
    - docker load -i ta_assignment_ui.tar
  script:
    - docker-compose run cypresstest npm run cypress:firefox 
  
Edge Test:
  stage: test
  environment: staging
  before_script:
    - apk add 'py-pip'
    - pip install 'docker-compose==1.22.0'
    - docker-compose version
    - docker load -i ta_assignment_apiservice.tar
    - docker load -i ta_assignment_ui.tar
  script:
    - docker-compose run cypresstestedge npm run cypress:edge       

publishing_docker:
  stage: publishing_docker
  environment: staging
  before_script:
    - apk add 'py-pip'
    - pip install 'docker-compose==1.22.0'
    - docker-compose version
    # Install CA certs, openssl to https downloads, python for gcloud sdk
    - apk add --update make ca-certificates openssl python
    - update-ca-certificates
    # Write our GCP service account private key into a file
    - echo $GCP_SA_KEY | base64 -d > ${HOME}/gcloud-service-key.json
    # Download and install Google Cloud SDK
    - wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz
    - tar zxvf google-cloud-sdk.tar.gz && ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true
    - google-cloud-sdk/bin/gcloud --quiet components update
    - google-cloud-sdk/bin/gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
    - docker login -u _json_key --password-stdin https://gcr.io < ${HOME}/gcloud-service-key.json
    - docker load -i ta_assignment_apiservice.tar
    - docker load -i ta_assignment_ui.tar
  script:  
    - docker tag ta_assignment_apiservice gcr.io/${GCLOUD_PROJECT_ID}/ta_assignment_apiservice
    - docker push gcr.io/${GCLOUD_PROJECT_ID}/ta_assignment_apiservice
    - docker tag ta_assignment_ui gcr.io/${GCLOUD_PROJECT_ID}/ta_assignment_ui
    - docker push gcr.io/${GCLOUD_PROJECT_ID}/ta_assignment_ui

deploy-prod:
    stage: deploy
    environment: production
    image: google/cloud-sdk
    before_script:
      # Write our GCP service account private key into a file
      - echo $GCP_SA_KEY | base64 -d > ${HOME}/gcloud-service-key.json
      # Download and install Google Cloud SDK
      - apt-get install -y wget
      - wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz
      - tar zxvf google-cloud-sdk.tar.gz && ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true
      - google-cloud-sdk/bin/gcloud --quiet components update
      - google-cloud-sdk/bin/gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
      - docker login -u _json_key --password-stdin https://gcr.io < ${HOME}/gcloud-service-key.json
    script:
        - kubectl apply -f deployment.yaml --record --kubeconfig ${HOME}/gcloud-service-key.json
```